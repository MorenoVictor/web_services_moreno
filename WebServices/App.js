import React from 'react';
import {ScrollView} from 'react-native';
import SingleView from './SingleView.js'
import Splash from './Splash.js'

const App = () => {
  return (
    <>
      <ScrollView>
         <SingleView/>
      </ScrollView>
    </>
  );
};

export default App;
